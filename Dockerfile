FROM denoland/deno:alpine-1.34.3

WORKDIR /app

COPY . /app

RUN deno cache main.ts

CMD ["run", "--allow-all", "main.ts"]